#include <iostream>
#include <string>
#include <cmath>
using namespace std;

string staple(string);
bool seq_val = false;
int stap_num;
double calculus(string);
double polynom(int, double, double, string);

int main()
{
    string enter;
    cout << "Enter expression:  ";
    getline(cin, enter);

    cout << calculus(enter) << endl;
    return 0;
}

double calculus(string expression)
{
    int num_seq = 0;
    double numbers[expression.length()], result = 0;

    for (int i = 0; i <= expression.length(); i++) // цикл для составления членов многочлена
    {
        if (expression[i] >= '0' && expression[i] <= '9') // поиск числа от 0 до 9 в массиве
        {
            int a = i, b;
            numbers[num_seq] = (double)expression[i] - 48;
            for (a += 1; expression[a] >= '0' && expression[a] <= '9'; a++) // сборка целой части числа
                numbers[num_seq] = numbers[num_seq] * 10 + (double)expression[a] - 48;

            if (expression[a] == '.' || expression[a] == ',') // сборка дробной части числа
                for (a += 1, b = 1; expression[a] >= '0' && expression[a] <= '9'; a++, b++)
                    numbers[num_seq] += ((double)expression[a] - 48) / pow(10, b);

            double num = polynom(i, numbers[num_seq - 1], numbers[num_seq], expression);
            numbers[num_seq] = 0;
            numbers[num_seq - seq_val] = num;

            num_seq = num_seq - seq_val + 1;
            seq_val = false;
            i = a--;
        }

        else if (expression[i] == '(')
        {
            stap_num = i;
            string value(staple(expression));    // функция staple оставляет лишнюю скобку,
            value[value.length() - 1] = '\0';    // которую надо убрать
            //cout << value << endl;
            numbers[num_seq] = calculus(value);

            double num = polynom(i, numbers[num_seq - 1], numbers[num_seq], expression);
            numbers[num_seq] = 0;
            numbers[num_seq - seq_val] = num;

            num_seq = num_seq - seq_val + 1;
            seq_val = false;
            i = stap_num;
        }
    }

    for (int i = 0; i < num_seq; i++) // сложение членов многочлена
        result += numbers[i];
    cout << result << endl;
    return result;
}

double polynom(int k, double numbers_1, double numbers_2, string expression)
{
    for (k -= 1; k >= 0 && expression[k] != '+'; k--) // определение членов многочлена
    {
        if (expression[k] == '-')
            numbers_2 *= -1;

        else if (expression[k] >= '0' && expression[k] <= '9')
            break;

        else if (expression[k] == '*')
        {
            seq_val = true;
            numbers_2 *= numbers_1;
            break;
        }
        else if (expression[k] == '/')
        {
            seq_val = true;
            numbers_2 = numbers_1 / numbers_2;
            break;
        }
        else if (expression[k] == '^')
        {
            seq_val = true;
            numbers_2 = pow(numbers_1, numbers_2);
            break;
        }
    }
    return numbers_2;
}

string staple(string enter)
{
    string expression;
    while (stap_num <= enter.length())
    {
        ++stap_num;
        expression += enter[stap_num];
        if (enter[stap_num] == '(')
            expression += staple(enter);
        else if (enter[stap_num] == ')')
            break;
    }
    return expression;
}