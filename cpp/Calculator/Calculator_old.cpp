#include <iostream>
#include <cmath>
using namespace std;

char enter[750];
double calculus(double);

int main()
{
    double result = 0;
    cout << "Enter expression:  ";
    cin.getline(enter, sizeof(enter));
    
    cout << calculus(result);
    return 0;
}

double calculus(double result)
{
    int size = 1, num_seq = 0;
    for (int i = 0; enter[i] != '\0'; i++)    // определение размера входного массива
        size++;
    char expression[size];
    double numbers[size];

    for (int i = 0; i < size; i++)    // запись глобального входного массива в локальный
        expression[i] = enter[i];

    for (int i = 0; i < size; i++)    // цикл для составления членов многочлена
    {
        if (expression[i] >= '0' && expression[i] <= '9')    // поиск числа от 0 до 9 в массиве
        {
            int a = i, b;
            numbers[num_seq] = (double)expression[i] - 48;
            for (a += 1; expression[a] >= '0' && expression[a] <= '9'; a++)   // сборка целой части числа
                numbers[num_seq] = numbers[num_seq] * 10 + (double)expression[a] - 48;

            if (expression[a] == '.' || expression[a] == ',')    // сборка дробной части числа
                for (a += 1, b = 1; expression[a] >= '0' && expression[a] <= '9'; a++, b++)
                    numbers[num_seq] += ((double)expression[a] - 48) / pow(10, b);

            for (int k = i - 1; k >= 0 && expression[k] != '+'; k--)   // определение членов многочлена
            {
                if (expression[k] == '-')
                    numbers[num_seq] *= -1;

                else if (expression[k] >= '0' && expression[k] <= '9')
                    break;

                else if (expression[k] == '*')
                {
                    num_seq--;
                    numbers[num_seq] *= numbers[num_seq + 1];
                    break;
                }
                else if (expression[k] == '/')
                {
                    num_seq--;
                    numbers[num_seq] /= numbers[num_seq + 1];
                    break;
                }
            }
            num_seq++;
            i = a--;
        }
    }

    for (int i = 0; i < num_seq; i++)    // сложение членов многочлена
        result += numbers[i];

    return result;
}