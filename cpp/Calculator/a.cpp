#include <iostream>
#include <string>
using namespace std;

string enter;
string staple();
int i = 5;

int main()
{
    cout << "Enter expression:  " << endl;
    getline(cin, enter);
    string result(staple());
    result[result.length() - 1] = '\n';
    cout << result;
}

string staple()
{
    string expression;
    while (i <= enter.length())
    {
        i += 1;
        expression += enter[i];
        if (enter[i] == '(')
            expression += staple();
        else if (enter[i] == ')')
            break;
    }
    return expression;
}