#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main()
{
    ifstream read("text.dat");
    ofstream write("text.dat", ios::app);
    string str;

    read >> str;
    if (str != "#DATA")
    {
        read.close();
        write << "#DATA\n" << endl;
    }
    while (str != "n")
    {
        cout << "\nService name:  ";
        getline(cin, str);
        write << str << " ";
        cout << "Login:  ";
        getline(cin, str);
        write << str << " ";
        cout << "Password:  ";
        getline(cin, str);
        write << str << endl;
        cout << "Continue? (n - no)  ";
        getline(cin, str);
        cout << endl;
    }

    write.close();
    return 0;
}