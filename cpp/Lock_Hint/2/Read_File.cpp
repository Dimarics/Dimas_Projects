#include <iostream>
#include <fstream>
#include <string>

using namespace std;

ifstream read("text.dat");
void skip_string();

int main()
{
    int ret = 0;
    string str, search;

    if (!read)
    {
        cout << "File not found!";
        ret = 1;
    }
    else
    {
        getline(cin, search);
        while (read && str != search && search != "SERVICE_NAME"&& search != "#DATA")
        {
            skip_string();
            read >> str;
        }
        if (str == search)
        {
            read >> str;
            cout << "Login:     " << str << endl;
            read >> str;
            cout << "Password:  " << str << endl;
        }
        else
            cout << "Service not found";

        ret = 0;
    }
    read.close();
    return ret;
}

void skip_string()
{
    char check;
    read.get(check);
    if (check == ' ')
        while (check != '\n' && check != '\0')
            read.get(check);
}