#include <stdio.h>
#include <stdlib.h>

int main()
{
    int biba, boba, a = 0, b = 0, c = 0, d = 0, e = 0, f = 0;
    for (biba = 1; biba <= 6000; biba++)
    {
        boba = 1 + rand() % 6;
        switch (boba)
        {
            case 1:
                ++a;
                break;
            case 2:
                ++b;
                break;
            case 3:
                ++c;
                break;
            case 4:
                ++d;
                break;
            case 5:
                ++e;
                break;
            case 6:
                ++f;
                break;
        }
    }
    printf("%s%13s\n", "Face", "Frequency");
    printf("    1%13d\n", a);
    printf("    1%13d\n", b);
    printf("    1%13d\n", c);
    printf("    1%13d\n", d);
    printf("    1%13d\n", e);
    printf("    1%13d\n", f);
    return 0;
}