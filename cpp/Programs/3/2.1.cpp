#include <stdio.h>

int main()
{
    int i, offset, b[] = {10, 20, 30, 40};
    int *bPtr = b;

    printf("Array b is printed with:\n"
           "Array subscript notation\n");

    for (i = 0; i <= 3; i++)
        printf("b[%d] = %d\n", i, b[i]);
        
    printf("\nPointer/offset notation where \n"
               "the pointer is the array name\n");

    for (offset = 0; offset <= 3; offset++)
        printf("*(b + %d) = %d\n", i, bPtr[i]);

    printf("\nPointer subscript notation\n");

    for (i = 0; i <= 3; i++)
        printf("bPtr[%d] = %d\n", i, bPtr[i]);
    printf("\nPointer/offset notation\n");

    for (offset = 0; offset <= 3; offset++)
        printf("*(b + %d) = %d\n", offset, *(bPtr + offset));
}