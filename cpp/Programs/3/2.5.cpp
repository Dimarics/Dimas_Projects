#include <stdio.h>
#define SIZE 10

void bubbleSort(int *, int);

int main()
{
    int i, a[SIZE] = {2, 6, 4, 8, 10, 12, 87, 68, 45, 35};

    printf("Data items in ascending order\n");

    for (i = 0; i <= SIZE - 1; i++)
        printf("%4d", a[i]);
    printf("\n");
    return 0;
}

void bubbleSort(int *array, int size)
{
    int pass, j;
    void swap(int *, int *);
    for (pass = 1; pass <=  size - 1; pass++)
        for (j = 0; j <= size - 2; j++)
        if (array[j] > &array[j + 1])
            swap(&array[j], &array[j]);
}